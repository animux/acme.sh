.TH ACME.SH "1" "February 2023" "acme.sh 3.0.5" "User Commands"
.SH NAME
acme.sh \- pure unix shell script implementing ACME client protocol 
.SH SYNOPSIS
.B acme.sh
\fI\,<command> \/\fR[\fI\,parameters \/\fR...]
.SH DESCRIPTION
acme.sh is a command-line utility for automating the process of setting up and
managing TLS/SSL certificates for web servers. It simplifies the process of
obtaining and renewing certificates from ZeroSSL, Let's Encrypt and other
Certificate Authorities.
.PP
The features of acme.sh includes:
.IP \(bu 4
Full ACME protocol implementation
.IP \(bu
Support ECDSA certs
.IP \(bu 
Support SAN and wildcard certs
.IP \(bu
Bash, dash and sh compatible
.IP \(bu
Purely written in Shell with no dependencies on python
.IP \(bu
Just one script to issue, renew and install your certificates automatically
.IP \(bu
DOES NOT require root/sudoer access
.IP \(bu
Docker ready
.IP \(bu
IPv6 ready
.IP \(bu
Cron job notifications for renewal or error etc.
.PP
.SH OPTIONS
.SS "Commands"
.TP
\fB\-h\fR, \fB\-\-help\fR
Show this help message.
.TP
\fB\-v\fR, \fB\-\-version\fR
Show version info.
.TP
\fB\-\-install\fR
Install acme.sh to your system.
.TP
\fB\-\-uninstall\fR
Uninstall acme.sh, and uninstall the cron job.
.TP
\fB\-\-issue\fR
Issue a cert.
.TP
\fB\-\-deploy\fR
Deploy the cert to your server.
.TP
\fB\-i\fR, \fB\-\-install\-cert\fR
Install the issued cert to apache/nginx or any other server.
.TP
\fB\-r\fR, \fB\-\-renew\fR
Renew a cert.
.TP
\fB\-\-renew\-all\fR
Renew all the certs.
.TP
\fB\-\-revoke\fR
Revoke a cert.
.TP
\fB\-\-remove\fR
Remove the cert from list of certs known to acme.sh.
.TP
\fB\-\-list\fR
List all the certs.
.TP
\fB\-\-info\fR
Show the acme.sh configs, or the configs for a domain with [\-d domain] parameter.
.TP
\fB\-\-to\-pkcs12\fR
Export the certificate and key to a pfx file.
.TP
\fB\-\-to\-pkcs8\fR
Convert to pkcs8 format.
.TP
\fB\-\-sign\-csr\fR
Issue a cert from an existing csr.
.TP
\fB\-\-show\-csr\fR
Show the content of a csr.
.TP
\fB\-ccr\fR, \fB\-\-create\-csr\fR
Create CSR, professional use.
.TP
\fB\-\-create\-domain\-key\fR
Create an domain private key, professional use.
.TP
\fB\-\-update\-account\fR
Update account info.
.TP
\fB\-\-register\-account\fR
Register account key.
.TP
\fB\-\-deactivate\-account\fR
Deactivate the account.
.TP
\fB\-\-create\-account\-key\fR
Create an account private key, professional use.
.TP
\fB\-\-install\-cronjob\fR
Install the cron job to renew certs, you don't need to call this. The 'install' command can automatically install the cron job.
.TP
\fB\-\-uninstall\-cronjob\fR
Uninstall the cron job. The 'uninstall' command can do this automatically.
.TP
\fB\-\-cron\fR
Run cron job to renew all the certs.
.TP
\fB\-\-set\-notify\fR
Set the cron notification hook, level or mode.
.TP
\fB\-\-deactivate\fR
Deactivate the domain authz, professional use.
.TP
\fB\-\-set\-default\-ca\fR
Used with '\-\-server', Set the default CA to use.
See:
.UR https://github.com/acmesh\-official/acme.sh/wiki/Server
.UE
.TP
\fB\-\-set\-default\-chain\fR
Set the default preferred chain for a CA.
See:
.UR https://github.com/acmesh\-official/acme.sh/wiki/Preferred\-Chain
.UE
.PP
.SS "Parameters"
.TP
\fB\-d\fR, \fB\-\-domain\fR <domain.tld>
Specifies a domain, used to issue, renew or revoke etc.
.TP
\fB\-\-challenge\-alias\fR <domain.tld>
The challenge domain alias for DNS alias mode.
See:
.UR https://github.com/acmesh\-official/acme.sh/wiki/DNS\-alias\-mode
.UE
.TP
\fB\-\-domain\-alias\fR <domain.tld>
The domain alias for DNS alias mode.
See:
.UR https://github.com/acmesh\-official/acme.sh/wiki/DNS\-alias\-mode
.UE
.TP
\fB\-\-preferred\-chain\fR <chain>
If the CA offers multiple certificate chains, prefer the chain with an issuer matching this Subject Common Name.
If no match, the default offered chain will be used. (default: empty)
See:
.UR https://github.com/acmesh\-official/acme.sh/wiki/Preferred\-Chain
.UE
.TP
\fB\-\-valid\-to\fR <date\-time>
Request the NotAfter field of the cert.
See:
.UR https://github.com/acmesh\-official/acme.sh/wiki/Validity
.UE
.TP
\fB\-\-valid\-from\fR <date\-time>
Request the NotBefore field of the cert.
See:
.UR https://github.com/acmesh\-official/acme.sh/wiki/Validity
.UE
.TP
\fB\-f\fR, \fB\-\-force\fR
Force install, force cert renewal or override sudo restrictions.
.TP
\fB\-\-staging\fR, \fB\-\-test\fR
Use staging server, for testing.
.TP
\fB\-\-debug\fR [0|1|2|3]
Output debug info. Defaults to 1 if argument is omitted.
.TP
\fB\-\-output\-insecure\fR
Output all the sensitive messages.
By default all the credentials/sensitive messages are hidden from the output/debug/log for security.
.TP
\fB\-w\fR, \fB\-\-webroot\fR <directory>
Specifies the web root folder for web root mode.
.TP
\fB\-\-standalone\fR
Use standalone mode.
.TP
\fB\-\-alpn\fR
Use standalone alpn mode.
.TP
\fB\-\-stateless\fR
Use stateless mode.
See:
.UR https://github.com/acmesh\-official/acme.sh/wiki/Stateless\-Mode
.UE
.TP
\fB\-\-apache\fR
Use apache mode.
.TP
\fB\-\-dns\fR [dns_hook]
Use dns manual mode or dns api. Defaults to manual mode when argument is omitted.
See:
.UR https://github.com/acmesh\-official/acme.sh/wiki/dnsapi
.UE
.TP
\fB\-\-dnssleep\fR <seconds>
The time in seconds to wait for all the txt records to propagate in dns api mode.
It's not necessary to use this by default, acme.sh polls dns status by DOH automatically.
.TP
\fB\-k\fR, \fB\-\-keylength\fR <bits>
Specifies the domain key length: 2048, 3072, 4096, 8192 or ec\-256, ec\-384, ec\-521.
.TP
\fB\-ak\fR, \fB\-\-accountkeylength\fR <bits>
Specifies the account key length: 2048, 3072, 4096
.TP
\fB\-\-log\fR [file]
Specifies the log file. Defaults to \fI~/.acme.sh/acme.sh.log\fR if argument is omitted.
.TP
\fB\-\-log\-level\fR <1|2>
Specifies the log level, default is 1.
.TP
\fB\-\-syslog\fR <0|3|6|7>
Syslog level.
.RS
.IP 0 4
disable syslog
.IP 3
error
.IP 6
info
.IP 7
debug
.RE
.TP
\fB\-\-eab\-kid\fR <eab_key_id>
Key Identifier for External Account Binding.
.TP
\fB\-\-eab\-hmac\-key\fR <eab_hmac_key>
HMAC key for External Account Binding.
.PP
These parameters are to install the cert to nginx/apache or any other server after issue/renew a cert:
.TP
\fB\-\-cert\-file\fR <file>
Path to copy the cert file to after issue/renew..
.TP
\fB\-\-key\-file\fR <file>
Path to copy the key file to after issue/renew.
.TP
\fB\-\-ca\-file\fR <file>
Path to copy the intermediate cert file to after issue/renew.
.TP
\fB\-\-fullchain\-file\fR <file>
Path to copy the fullchain cert file to after issue/renew.
.TP
\fB\-\-reloadcmd\fR <command>
Command to execute after issue/renew to reload the server.
.TP
\fB\-\-server\fR <server_uri>
ACME Directory Resource URI. (default:
.UR https://acme.zerossl.com/v2/DV90
.UE )
See:
.UR https://github.com/acmesh\-official/acme.sh/wiki/Server
.UE
.TP
\fB\-\-accountconf\fR <file>
Specifies a customized account config file.
.TP
\fB\-\-home\fR <directory>
Specifies the home dir for acme.sh.
.TP
\fB\-\-cert\-home\fR <directory>
Specifies the home dir to save all the certs, only valid for '\-\-install' command.
.TP
\fB\-\-config\-home\fR <directory>
Specifies the home dir to save all the configurations.
.TP
\fB\-\-useragent\fR <string>
Specifies the user agent string. it will be saved for future use too.
.TP
\fB\-m\fR, \fB\-\-email\fR <email>
Specifies the account email, only valid for the '\-\-install' and '\-\-update\-account' command.
.TP
\fB\-\-accountkey\fR <file>
Specifies the account key path, only valid for the '\-\-install' command.
.TP
\fB\-\-days\fR <ndays>
Specifies the days to renew the cert when using '\-\-issue' command. The default value is 60 days. If you supply a negative value, it will be interpreted as days before the expire date.
.TP
\fB\-\-httpport\fR <port>
Specifies the standalone listening port. Only valid if the server is behind a reverse proxy or load balancer.
.TP
\fB\-\-tlsport\fR <port>
Specifies the standalone tls listening port. Only valid if the server is behind a reverse proxy or load balancer.
.TP
\fB\-\-local\-address\fR <ip>
Specifies the standalone/tls server listening address, in case you have multiple ip addresses.
.TP
\fB\-\-listraw\fR
Only used for '\-\-list' command, list the certs in raw format.
.TP
\fB\-se\fR, \fB\-\-stop\-renew\-on\-error\fR
Only valid for '\-\-renew\-all' command. Stop if one cert has error in renewal.
.TP
\fB\-\-insecure\fR
Do not check the server certificate, in some devices, the api server's certificate may not be trusted.
.TP
\fB\-\-ca\-bundle\fR <file>
Specifies the path to the CA certificate bundle to verify api server's certificate.
.TP
\fB\-\-ca\-path\fR <directory>
Specifies directory containing CA certificates in PEM format, used by wget or curl.
.TP
\fB\-\-no\-cron\fR
Only valid for '\-\-install' command, which means: do not install the default cron job.
In this case, the certs will not be renewed automatically.
.TP
\fB\-\-no\-profile\fR
Only valid for '\-\-install' command, which means: do not install aliases to user profile.
.TP
\fB\-\-no\-color\fR
Do not output color text.
.TP
\fB\-\-force\-color\fR
Force output of color text. Useful for non\-interactive use with the aha tool for HTML E\-Mails.
.TP
\fB\-\-ecc\fR
Specifies to use the ECC cert. Valid for '\-\-install\-cert', '\-\-renew', '\-\-revoke', '\-\-to\-pkcs12' and '\-\-create\-csr'
.TP
\fB\-\-csr\fR <file>
Specifies the input csr.
.TP
\fB\-\-pre\-hook\fR <command>
Command to be run before obtaining any certificates.
.TP
\fB\-\-post\-hook\fR <command>
Command to be run after attempting to obtain/renew certificates. Runs regardless of whether obtain/renew succeeded or failed.
.TP
\fB\-\-renew\-hook\fR <command>
Command to be run after each successfully renewed certificate.
.TP
\fB\-\-deploy\-hook\fR <hookname>
The hook file to deploy cert
.TP
\fB\-\-ocsp\fR, \fB\-\-ocsp\-must\-staple\fR
Generate OCSP\-Must\-Staple extension.
.TP
\fB\-\-always\-force\-new\-domain\-key\fR
Generate new domain key on renewal. Otherwise, the domain key is not changed by default.
.TP
\fB\-\-listen\-v4\fR
Force standalone/tls server to listen at ipv4.
.TP
\fB\-\-listen\-v6\fR
Force standalone/tls server to listen at ipv6.
.TP
\fB\-\-openssl\-bin\fR <file>
Specifies a custom openssl bin location.
.TP
\fB\-\-use\-wget\fR
Force to use wget, if you have both curl and wget installed.
.TP
\fB\-\-yes\-I\-know\-dns\-manual\-mode\-enough\-go\-ahead\-please\fR
Force use of dns manual mode.
See:
.UR https://github.com/acmesh\-official/acme.sh/wiki/dns\-manual\-mode
.UE
.TP
\fB\-\-notify\-level\fR <0|1|2|3>
Set the notification level:  Default value is 2.
.RS
.IP 0 4
disabled, no notification will be sent.
.IP 1
send notifications only when there is an error.
.IP 2
send notifications when a cert is successfully renewed, or there is an error.
.IP 3
send notifications when a cert is skipped, renewed, or error.
.RE
.TP
\fB\-\-notify\-mode\fR <0|1>
Set notification mode. Default value is 0.
.RS
.IP 0 4
Bulk mode. Send all the domain's notifications in one message(mail).
.IP 1
Cert mode. Send a message for every single cert.
.RE
.TP
\fB\-\-notify\-hook\fR <hookname>
Set the notify hook
.TP
\fB\-\-revoke\-reason\fR <0\-10>
The reason for revocation, can be used in conjunction with the '\-\-revoke' command.
See:
.UR https://github.com/acmesh\-official/acme.sh/wiki/revokecert
.UE
.TP
\fB\-\-password\fR <password>
Add a password to exported pfx file. Use with \fB\-\-to\-pkcs12\fR.
.PP
.SH SEE ALSO
.UR https://github.com/acmesh\-official/acme.sh
.UE
